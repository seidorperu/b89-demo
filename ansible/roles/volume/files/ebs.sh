#!/bin/bash

mkdir -p /usr/sap
mkdir -p /backup
mkdir -p /hana/log
mkdir -p /hana/data
mkdir -p /hana/shared


fdisk -l | grep "Disk /dev/nvme" | grep -v "nvme0n1" | cut -d ":" -f1 | cut -d " " -f2 > volumes.txt

while read line
do
    EBS_ID=`nvme id-ctrl -v ${line} | grep "0000:" | cut -d '"' -f2 | cut -d "." -f1`
    mkfs.xfs ${line}
    EBS_UUID=`blkid -s UUID -o value ${line}`

    if [ ${EBS_ID} = "xvdf" ]; then
        echo "UUID=${EBS_UUID} /usr/sap xfs defaults 0 0" >> /etc/fstab
    fi
    if [ ${EBS_ID} = "xvdg" ]; then
        echo "UUID=${EBS_UUID} /backup xfs defaults 0 0" >> /etc/fstab
    fi
    if [ ${EBS_ID} = "xvdh" ]; then
        echo "UUID=${EBS_UUID} /hana/log xfs defaults 0 0" >> /etc/fstab
    fi
    if [ ${EBS_ID} = "xvdi" ]; then
        echo "UUID=${EBS_UUID} /hana/data xfs defaults 0 0" >> /etc/fstab
    fi
    if [ ${EBS_ID} = "xvdj" ]; then
        echo "UUID=${EBS_UUID} /hana/shared xfs defaults 0 0" >> /etc/fstab
    fi
    if [ ${EBS_ID} = "xvdk" ]; then
        echo "UUID=${EBS_UUID} /sapmnt xfs defaults 0 0" >> /etc/fstab
    fi
done <  volumes.txt