#!/bin/bash

echo "Script solo para Suse, ejecutarlo solo una vez"
sleep 3
echo "--Seteando timezone Lima--"
sleep 2
timedatectl set-timezone America/Lima

echo "ok"

sleep 2
echo ""
echo "--Ejecutando parches de seguridad--"
sleep 2

zypper patch --category security -y
echo "ok"
sleep 2
echo ""

echo "--Validando si todos los discos estan en el fstaban con su UUID:--"
sleep 2

cat /etc/fstab
sleep 5
echo ""
echo "--Deshabilitando los parametros de red:--"
sleep 2

echo "net.ipv4.ip_forward = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.send_redirects = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.default.send_redirects = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.secure_redirects = 0" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf

sysctl -p
echo "ok"
sleep 2
echo ""
echo "--Desactivando el firewall:--"
sleep 2

systemctl stop SuSEfirewall2
systemctl disable SuSEfirewall2

echo "ok"
sleep 2
echo ""
echo "--configurando parametros de SSH:--"
sleep 2

sed -i 's/#PermitRootLogin/PermitRootLogin/' /etc/ssh/sshd_config
sed -i "s/#PermitEmptyPassword/PermitEmptyPassword/" /etc/ssh/sshd_config
sed -i 's/#X11Forwarding/X11Forwarding/' /etc/ssh/sshd_config

sed -i '/^PermitRootLogin/s/yes/no/' /etc/ssh/sshd_config
sed -i '/^PermitEmptyPassword/s/yes/no/' /etc/ssh/sshd_config
sed -i '/^X11Forwarding/s/yes/no/' /etc/ssh/sshd_config

echo "ok"
sleep 2
echo ""
echo "--Aplicando parametros de password para usuarios no root--"
pam-config -a --cracklib --cracklib-minlen=16 --cracklib-lcredit=-1 --cracklib-ucredit=-1 --cracklib-dcredit=-1 --cracklib-ocredit=-1
sleep 2

echo "ok"
sleep 2
echo ""
echo "--Agregando marca de tiempo en el bash global--"
sleep 2

echo 'export HISTTIMEFORMAT="%d/%m/%y %T "' >> ~/.bash_profile
sleep 2
echo ""
echo "ok, reinicio de servidor pendiente"
sleep 3