#!/bin/bash
sudo killall -9 zypper
sudo zypper addrepo https://download.opensuse.org/repositories/devel:languages:python/SLE_15_SP2/devel:languages:python.repo
sudo zypper ar http://download.opensuse.org/repositories/systemsmanagement/SLE_15_SP1/systemsmanagement.repo
sudo zypper install -y python-passlib
sudo zypper --gpg-auto-import-keys  install -y ansible

