#!/bin/bash

mkdir -p /usr/sap
mkdir -p /backup
mkdir -p /hana/log
mkdir -p /hana/data
mkdir -p /hana/shared

CONTEO=0;

fdisk -l | grep "Disk /dev/nvme" | grep -v "nvme5n1" | cut -d ":" -f1 | cut -d " " -f2 > volumes.txt

while read line
do
    EBS_ID=`nvme id-ctrl -v ${line} | grep "0000:" | cut -d '"' -f2 | cut -d "." -f1`
    mkfs.xfs ${line}
    EBS_UUID=`blkid -s UUID -o value ${line}`
    CONTEO=$((CONTEO+1))
case $CONTEO in
  1)
  echo "UUID=${EBS_UUID} /usr/sap xfs defaults 0 0" >> /etc/fstab
  ;;
  2)
    echo "UUID=${EBS_UUID} /backup xfs defaults 0 0" >> /etc/fstab
  ;;
  3)
    echo "UUID=${EBS_UUID} /hana/log xfs defaults 0 0" >> /etc/fstab
  ;;
  4)
    echo "UUID=${EBS_UUID} /hana/data xfs defaults 0 0" >> /etc/fstab
  ;;
  5)
    echo "UUID=${EBS_UUID} /hana/shared xfs defaults 0 0" >> /etc/fstab
  ;;

esac

done <  volumes.txt

mount -a

sudo systemctl status amazon-ssm-agent
