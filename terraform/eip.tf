#####Elastic ip Terminal server ###########

resource "aws_eip" "eip-terminal-qa" {
  vpc = true
}

resource "aws_eip_association" "eip_terminal-qa" {
  instance_id = aws_instance.TERMINAL-SERVER.id
  allocation_id = aws_eip.eip-terminal-qa.id
}
####elastic ip bastion server #####
resource "aws_eip" "eip-bastion-qa" {
  vpc = true
}

resource "aws_eip_association" "eip_bastion-qa" {
  instance_id = aws_instance.bastion.id
  allocation_id = aws_eip.eip-bastion-qa.id
}

##ELASTICIP NGW##
resource "aws_eip" "eip_nat-gw" {
  vpc = true
  associate_with_private_ip = var.eip_association_ngw_qa
  tags = {
    Name = "eip-hana-us2-qa-ngw"
  }
}
