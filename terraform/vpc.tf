<<<<<<< HEAD
provider "aws" {
  region = var.region
}
#######################VPC#################################
=======
#######################VPC############################################
>>>>>>> 53b1cd4df89a2d68d753f64aa6a570e1286d09a4
resource "aws_vpc" "hana-vpc" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = "true"

  tags = {
    Name     = var.vpc_name
    Location = var.vpc_location
  }
}

################################Subnet private################################3

resource "aws_subnet" "subnet_private_1" {
  availability_zone = var.az1
  vpc_id            = aws_vpc.hana-vpc.id
  cidr_block        = var.az1_cidr_private

  tags = {
    Name = var.az1_tag_name_private
  }
}

resource "aws_subnet" "subnet_private_2" {
  availability_zone = var.az2
  vpc_id            = aws_vpc.hana-vpc.id
  cidr_block        = var.az2_cidr_private


  tags = {
    Name = var.az2_tag_name_private
  }
}

resource "aws_subnet" "subnet_private_3" {
  availability_zone = var.az3
  vpc_id            = aws_vpc.hana-vpc.id
  cidr_block        = var.az3_cidr_private

  tags = {
    Name = var.az3_tag_name_private
  }
}

#####################################Subnet public###########################################

resource "aws_subnet" "subnet_public_1" {
  availability_zone = var.az1
  vpc_id            = aws_vpc.hana-vpc.id
  cidr_block        = var.az1_cidr_public

  tags = {
    Name = var.az1_tag_name_public
  }
}

resource "aws_subnet" "subnet_public_2" {
  availability_zone = var.az2
  vpc_id            = aws_vpc.hana-vpc.id
  cidr_block        = var.az2_cidr_public

  tags = {
    Name = var.az2_tag_name_public
  }
}

resource "aws_subnet" "subnet_public_3" {
  availability_zone = var.az3
  vpc_id            = aws_vpc.hana-vpc.id
  cidr_block        = var.az3_cidr_public

  tags = {
    Name = var.az3_tag_name_public
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.hana-vpc.id
  tags = {
    Name = var.igw_hana

  }
}

#####################NAT Gateway############################################

resource "aws_nat_gateway" "gw" {

  allocation_id = aws_eip.eip_nat-gw.id
  subnet_id     = aws_subnet.subnet_public_1.id
  tags = {
    Name = var.ngw_hana
  }
}

##############################Route table private#################################
resource "aws_route_table" "route_table_private" {
  vpc_id = aws_vpc.hana-vpc.id

  ##peering cidr block del destino
  route {
    cidr_block                = var.destino_peer_cidr
    vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  }
  ##peering cidr block del destino2
  route {
    cidr_block                = var.destino_peer2_cidr
    vpc_peering_connection_id = aws_vpc_peering_connection.peer2.id
  }
  tags = {
    Name = var.route_table_tag_private
  }
}
###################################Route table public#########################################

resource "aws_route_table" "route_table_public" {
  vpc_id = aws_vpc.hana-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  ##peering cidr block del destino
  route {
    cidr_block                = var.destino_peer_cidr
    vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  }
  ##peering cidr block del destino2
  route {
    cidr_block                = var.destino_peer2_cidr
    vpc_peering_connection_id = aws_vpc_peering_connection.peer2.id
  }
  tags = {
    Name = var.route_table_tag_public
  }
}


#Route-association-private

resource "aws_route_table_association" "subnet_private_1" {
  subnet_id      = aws_subnet.subnet_private_1.id
  route_table_id = aws_route_table.route_table_private.id
}

resource "aws_route_table_association" "subnet_private_2" {
  subnet_id      = aws_subnet.subnet_private_2.id
  route_table_id = aws_route_table.route_table_private.id
}

resource "aws_route_table_association" "subnet_private_3" {
  subnet_id      = aws_subnet.subnet_private_3.id
  route_table_id = aws_route_table.route_table_private.id
}

#Route-association-public

resource "aws_route_table_association" "subnet_public_1" {
  subnet_id      = aws_subnet.subnet_public_1.id
  route_table_id = aws_route_table.route_table_public.id
}

resource "aws_route_table_association" "subnet_public_2" {
  subnet_id      = aws_subnet.subnet_public_2.id
  route_table_id = aws_route_table.route_table_public.id
}

resource "aws_route_table_association" "subnet_public_3" {
  subnet_id      = aws_subnet.subnet_public_3.id
  route_table_id = aws_route_table.route_table_public.id
}

