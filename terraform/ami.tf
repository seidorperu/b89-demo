data "aws_ami" "suse" {
  most_recent = true

  filter {
    name   = "name"
    values = ["packer-hana-b89"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["116332599801"] # Canonical
}

data "aws_ami" "windows" {
  most_recent = true
  filter {
    name   = "name"
    values = ["Windows_Server-2016-English-Full-Base-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["801119661308"] # Canonical
}