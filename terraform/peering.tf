
###################
#provider "aws" {
#  region = var.region
#
# Requester's credentials.
#}

provider "aws" {
  alias  = "peer"
  region = var.destino_peer_region

  # Accepter's credentials.
}

resource "aws_vpc" "peer" {
  provider   = aws.peer
  cidr_block = var.destino_peer_cidr
}

data "aws_caller_identity" "peer" {
  provider = aws.peer
}

# Requester's side of the connection.
resource "aws_vpc_peering_connection" "peer" {
  vpc_id        = aws_vpc.hana-vpc.id
  peer_vpc_id   = var.destino_vpc_id
  peer_owner_id = var.destino_account_id
  peer_region   = var.destino_peer_region
  auto_accept   = false

  tags = {
    Side = "opetech-request"
  }
}

# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "peer" {
  provider                  = aws.peer
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  auto_accept               = true

  tags = {
    Side = "seidor-tech-accepter"
  }
}