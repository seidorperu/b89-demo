
###################
#provider "aws" {
#  region = var.region
#
# Requester's credentials.
#}

provider "aws" {
  alias  = "peer2"
  region = var.destino_peer2_region

  # Accepter's credentials.
}

resource "aws_vpc" "peer2" {
  provider   = aws.peer2
  cidr_block = var.destino_peer2_cidr
}

data "aws_caller_identity" "peer2" {
  provider = aws.peer2
}

# Requester's side of the connection.
resource "aws_vpc_peering_connection" "peer2" {
  vpc_id        = aws_vpc.hana-vpc.id
  peer_vpc_id   = var.destino_vpc2_id
  peer_owner_id = var.destino_account2_id
  peer_region   = var.destino_peer2_region
  auto_accept   = false

  tags = {
    Side = "opetech-request"
  }
}

# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "peer2" {
  provider                  = aws.peer2
  vpc_peering_connection_id = aws_vpc_peering_connection.peer2.id
  auto_accept               = true

  tags = {
    Side = "seidor-tech-accepter"
  }
}