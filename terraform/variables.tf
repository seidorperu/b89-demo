<<<<<<< HEAD
variable "region" {default = "us-east-2"}
variable "vpc_name" {default = "vpc-hana-us2-qa"}
variable "vpc_cidr" { default = "192.170.0.0/16"}
variable "vpc_location" { default = "vpc-hana-us2-qa"}
variable "az1" { default = "us-east-2a"}
variable "az2" { default = "us-east-2b"}
variable "az3" { default = "us-east-2c"}
variable "private_terminal_ip" { default = "192.170.1.37" }
variable "private_bastion_ip" { default = "192.170.10.37" }
variable "private_hana_ip" { default = "192.170.10.38" }
variable "hana-key" { default = "test-hana" }
=======
variable "region" {
  default = "us-east-2"
}
variable "vpc_name" {
  default = "vpc-hana-us2-qa"
}

variable "vpc_cidr" {
  default = "192.170.0.0/16"
}

variable "vpc_location" {
  default = "vpc-hana-us2-qa"
}

###aqui se pone el vpc id de la cuenta con la cual se quiere hacer el peering
#de cuenta seidor-tech region us-east-2
variable "destino_vpc_id" {
  default = "vpc-14a15e7d"
}

variable "destino_peer_region" {
  default = "us-east-2"
}

variable "destino_peer_cidr" {
  default = "172.31.0.0/16"
}

variable "destino_account_id" {
  default = "081713589812"
}

###aqui se pone el vpc id de la segunda cuenta con la cual se quiere hacer el peering
#de mi cuenta luhercen region us-east-1
variable "destino_vpc2_id" {
  default = "vpc-0789a0e271a850fc7"
}

variable "destino_peer2_region" {
  default = "us-east-1"
}

variable "destino_peer2_cidr" {
  default = "10.10.0.0/16"
}

variable "destino_account2_id" {
  default = "498681748455"
}

####
variable "az1" {
  default = "us-east-2a"
}

variable "az2" {
  default = "us-east-2b"
}

variable "az3" {
  default = "us-east-2c"
}

variable "private_terminal_ip" {
  default = "192.170.1.37"
}

variable "private_bastion_ip" {
  default = "192.170.10.37"
}

variable "private_hana_ip" {
  default = "192.170.10.38"
}

variable "key_name" {
  default = "test-hana"
}
>>>>>>> 53b1cd4df89a2d68d753f64aa6a570e1286d09a4
##################################VARIABLES AZ1############################################

############Private##########################

variable "az1_cidr_private" { default = "192.170.10.0/24"}
variable "az1_tag_name_private" { default = "subnet-us2a-hana-qa-private" }

############Public#####################

variable "az1_cidr_public" { default = "192.170.1.0/24" }
variable "az1_tag_name_public" { default = "subnet-us2a-hana-qa-public" }

##################################VARIABLES AZ2############################################

############Private#####################
variable "az2_cidr_private" { default = "192.170.11.0/24"}
variable "az2_tag_name_private" { default = "subnet-us2b-hana-qa-private" }
############Public#####################
variable "az2_cidr_public" { default = "192.170.2.0/24" }
variable "az2_tag_name_public" { default = "subnet-us2b-hana-qa-public" }

##################################VARIABLES AZ3############################################

############Private#####################
variable "az3_cidr_private" { default = "192.170.12.0/24" }
variable "az3_tag_name_private" { default = "subnet-us2c-hana-qa-private" }
############Public#####################
variable "az3_cidr_public" { default = "192.170.3.0/24" }
variable "az3_tag_name_public" { default = "subnet-us2c-hana-qa-public" }

############################VARIABLES IGW#####################################################

variable "igw_hana" { default = "igw-hana-us2-qa" }

############################VARIABLES NGW#####################################################
variable "ngw_hana" { default = "ngw-hana-us2-qa" }

##################################VARIABLES ROUTE_TABLE############################################

############Private#####################

variable "route_table_tag_private" { default = "rtb-hana-us2-qa-private" }

############Public#####################
variable "route_table_tag_public" { default = "rtb-hana-us2-qa-public" }

#####################################Sistemas-Operativos######################################
variable "ami_windows" {
  default = "ami-0952fb5203ddacf5c"
}

######LAUNCH CONFIGURATION VARIABLES TERMINAL NatInstance##########################

variable "instance-type-terminal-server" { default = "t2.micro" }

#LAUNCH CONFIGURATION VARIABLES BASTION
variable "instance_type_bastion" { default = "t2.micro" }

#LAUNCH CONFIGURATION VARIABLES HANA SERVER

variable "instance-type-hana" { default = "t3.medium" }

#Variable eip NAT#
variable "eip_association_ngw_qa" { default = "eip-ngw" }
