#/*
resource "aws_instance" "TERMINAL-SERVER" {
  ami = data.aws_ami.windows.id
  instance_type = var.instance-type-terminal-server
  vpc_security_group_ids = [aws_security_group.sg-terminal.id]
  subnet_id = aws_subnet.subnet_public_1.id
  associate_public_ip_address = true
  private_ip = var.private_terminal_ip
  root_block_device { volume_size = 192 }
  key_name = var.hana-key
  depends_on = [aws_internet_gateway.gw]
  user_data = file("windows_script.ps1")
  tags = {
    Name= "ec2-test-us2-qa-terminal"
    Env= "terminal-server"
  }
}


resource "aws_instance" "bastion" {
  ami = data.aws_ami.windows.id
  instance_type = var.instance_type_bastion
  vpc_security_group_ids = [aws_security_group.sg-bastion.id]
  subnet_id = aws_subnet.subnet_private_1.id
  associate_public_ip_address = true
  private_ip = var.private_bastion_ip
  root_block_device { volume_size = 100 }
  key_name = var.hana-key
  depends_on = [aws_internet_gateway.gw]
  user_data = file("windows_script.ps1")
  tags = {
    Name= "ec2-test-us2-qa-bastion"
    Env= "bastion"
  }
}
#*/

resource "aws_instance" "hana" {
  ami = data.aws_ami.suse.id
  instance_type = var.instance-type-hana
  vpc_security_group_ids = [aws_security_group.sg-hana.id]
  subnet_id = aws_subnet.subnet_private_1.id
  iam_instance_profile = aws_iam_instance_profile.ssm_profile.name
  private_ip = var.private_hana_ip
  key_name = var.hana-key
  depends_on = [aws_internet_gateway.gw]
  user_data = file("script.sh")
  tags = {
    Name= "ec2-test-us2-qa-hana"
    Env= "hana"
  }
}

#*/
