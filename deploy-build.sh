#!/bin/bash
cd packer
packer build main.json
cd ../terraform
terraform init; terraform plan; terraform apply -auto-approve;
